const API_URL = 'https://express-app-bsa2.herokuapp.com/';

function callApi(endpoind, method) {
  const url = API_URL + endpoind;
  const options = {
    ...method,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  };

  return fetch(url, options)
    .then(response => response.ok ? response.json() : Promise.reject(Error('Failed to load')))
    .catch(error => {
      throw error;
    });
}

async function getData(urlEndpoint){
  try {
    return await callApi(urlEndpoint, { method: 'GET' });
  } catch (error) {
    throw error;
  }
}

async function putData(urlEndpoint, body){
  try {
    return await callApi(urlEndpoint, {
      method: 'PUT',
      body: JSON.stringify(body)
    });
  } catch (error) {
    throw error;
  }
}

async function deleteData(urlEndpoint){
  try {
    return await callApi(urlEndpoint, { method: 'DELETE' });
  } catch (error) {
    throw error;
  }
}

export {
  getData,
  putData,
  deleteData
}