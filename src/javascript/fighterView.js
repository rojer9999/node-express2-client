import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source, _id } = fighter;
    const nameElement = this.createName(name, _id);
    const imageElement = this.createImage(source);

    this.element = this.createElement({
      tagName: 'div',
      classNames: ['fighter', 'nes-btn', 'is-primary']
    });
    this.element.append(imageElement, nameElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  createName(name, _id) {
    const nameElement = this.createElement({
      tagName: 'span',
      classNames: ['name'],
      attributes: {id: `fighter-${_id}`}
    });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      classNames: ['fighter-image'],
      attributes
    });

    return imgElement;
  }
}

export default FighterView;