import { getData, putData, deleteData } from '../helpers/apiHelper';

class FighterService {
  getFighters() {
    return getData('fighter');
  }

  getFighterDetails(_id) {
    return getData(`fighter/${_id}`);
  }

  putFighterDetails(_id, body) {
    return putData(`fighter/${_id}`, body);
  }

  deleteFighter(_id) {
    return deleteData(`fighter/${_id}`);
  }
}

export const fighterService = new FighterService();
