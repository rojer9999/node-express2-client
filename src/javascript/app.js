import FightersView from './fightersView';
import { fighterService } from './services/fightersService';

class App {
  constructor(id) {
    this.startApp(id);
  }

  static rootElement = document.getElementById('root');
  static loadingElement = document.getElementById('loading-overlay');

  async startApp(id) {
    try {
      App.loadingElement.style.visibility = 'visible';
      
      let fighters = await fighterService.getFighters()
        .then(data => data.filter(fighter => fighter._id !== id));
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      root.innerHTML = '';

      App.rootElement.appendChild(fightersElement);

      const click = () => document.getElementById('click').play();
      document.addEventListener('click', click, false)
    } catch (error) {
      console.warn(error);
      App.rootElement.innerText = 'Failed to load data';
    } finally {
      App.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;